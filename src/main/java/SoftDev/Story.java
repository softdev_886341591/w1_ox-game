/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package SoftDev;

/**
 *
 * @author acer
 */
public class Story {
    private int o = 0;
    private int x = 0;
    private int draw = 0;
    
    public void updateStory(char winner){
        if(winner == 'O'){
            o++;
        } else if (winner == 'X') {
            x++;
        } else {
            draw++;
        }
    }
    public int getWinO(){
        return o;
    }
    public int getWinx(){
        return x;
    }
    public int getdraw(){
        return draw;
    }
    
}
